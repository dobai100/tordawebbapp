package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class KulturnoUmetnickoDrustvoPetefiSandorController {

	@RequestMapping(value = "/kulturno_umetnicko_drustvo_petefi_sandor.html")
	public String showKulturnoUmetnickoDrustvoPetefiSandor(Model model) {
		return "kulturno_umetnicko_drustvo_petefi_sandor";
	}
}
