package org.training.torda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.training.torda.repository.UserRepository;

@Controller
public class UserListController {
	
	@Autowired
    private UserRepository userRepository;

	@RequestMapping(value = "/userlist.html")
	public String displayUserList(Model model) {
		model.addAttribute("users", userRepository.findAll());
		return "userlist";
	}
}
