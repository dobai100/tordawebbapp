package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ONamaController {

	@RequestMapping(value = "/o_nama.html")
	public String showONama(Model model) {
		return "o_nama";
	}
}
