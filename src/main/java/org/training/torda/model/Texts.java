package org.training.torda.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Texts {

	@Size(max = 36)
	private String textsId;

	@NotNull
	private String texts;
	
	@NotNull
	private String articleId;
	
	public Texts() {
		
	}
	
	public Texts(String textsId, String texts, String articleId) {
		this.textsId= textsId;
		this.texts = texts;
		this.articleId = articleId;
	}
	
	public String getTextsId() {
        return textsId;
    }

    public void setTextsId(String textsId) {
        this.textsId = textsId;
    }
    
    public String getTexts() {
    	return texts;
    }
    
    public void setTexts(String texts) {
    	this.texts = texts;
    }
    
    public String getArticleId() {
    	return articleId;
    }
    
    public void setArticleId(String articleId) {
    	this.articleId = articleId;
    }
    
    @Override
    public String toString() {
        return "User [textsId=" + textsId + ", texts=" + texts + ", articleId=" + articleId + "]";
    }
}
