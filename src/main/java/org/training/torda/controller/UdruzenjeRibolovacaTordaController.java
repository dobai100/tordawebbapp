package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UdruzenjeRibolovacaTordaController {

	@RequestMapping(value = "/udruzenje_ribolovaca_torda.html")
	public String showUdruzenjeRibolovacaTorda(Model model) {
		return "udruzenje_ribolovaca_torda";
	}
}
