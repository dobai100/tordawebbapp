package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BratimljenaMestaController {

	@RequestMapping(value = "/bratimljena_mesta.html")
	public String showBratimljenaMesta(Model model) {
		return "bratimljena_mesta";
	}
}
