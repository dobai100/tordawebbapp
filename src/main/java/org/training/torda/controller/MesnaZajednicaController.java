package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MesnaZajednicaController {

	@RequestMapping(value = "/mesnazajednica.html")
	public String showMesnaZajednica(Model model) {
		return "mesnazajednica";
	}
}
