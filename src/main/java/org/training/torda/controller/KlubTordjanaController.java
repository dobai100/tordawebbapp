package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class KlubTordjanaController {

	@RequestMapping(value = "/klub_tordjana.html")
	public String showKlubTordjana(Model model) {
		return "klub_tordjana";
	}
}
