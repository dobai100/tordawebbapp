package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DobaiController {

	@RequestMapping(value = "/dobai.html")
	public String showDobai(Model model) {
		return "dobai";
	}
}
