package org.training.torda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.training.torda.repository.ArticleRepository;

@Controller
@RequestMapping("/")
public class IndexController {
	
	@Autowired
    private ArticleRepository articleRepository;
	
	@RequestMapping(produces = "text/plain; charset=UTF-8")
	public String showIndex(Model model) {
		
		model.addAttribute("texts", articleRepository.findTextsForArticleId());
		model.addAttribute("pictures", articleRepository.findPictureForArticleId());
		model.addAttribute("articles", articleRepository.findLastFive());
		return "index";
	}

}
