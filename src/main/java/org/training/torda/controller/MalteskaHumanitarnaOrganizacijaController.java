package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MalteskaHumanitarnaOrganizacijaController {

	@RequestMapping(value = "/malteska_humanitarna_organizacija.html")
	public String showMalteskaHumanitarnaOrganizacija(Model model) {
		return "malteska_humanitarna_organizacija";
	}
}
