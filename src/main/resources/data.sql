INSERT INTO "user" (id, firstName, lastName, loginName, password, email, birthday) 
VALUES 
('1', 'Dobai', 'Robert', 'dobai', 'dobai', 'dobai@dobai.hu', '1990-08-01');

INSERT INTO article (id, articleName)
VALUES
('150', 'Útjavítások');

INSERT INTO article (id, articleName)
VALUES
('149', 'Böllérnapok Balástyán');

INSERT INTO article (id, articleName)
VALUES
('148', 'Első osztályos tanúlók ösztöndíj átadása');

INSERT INTO article (id, articleName)
VALUES
('147', 'A Kukoricafesesztivál tervezett programja 2017.09.16-án');

INSERT INTO article (id, articleName)
VALUES
('146', 'Aracs');

INSERT INTO article (id, articleName)
VALUES
('145', 'Értesítés');

INSERT INTO article (id, articleName)
VALUES
('144', 'Pacséri Falunap');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('1', '../static/images/utjavitasok1_2017.jpg', '/images/utjavitasok1_2017.jpg', '150');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('2', '../static/images/utjavitasok2_2017.jpg', '/images/utjavitasok2_2017.jpg', '150');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('3', '../static/images/utjavitasok3_2017.jpg', '/images/utjavitasok3_2017.jpg', '150');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('4', '../static/images/utjavitasok4_2017.jpg', '/images/utjavitasok4_2017.jpg', '150');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('5', '../static/images/bollernapok1_2017.jpg', '/images/bollernapok1_2017.jpg', '149');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('6', '../static/images/bollernapok2_2017.jpg', '/images/bollernapok2_2017.jpg', '149');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('7', '../static/images/bollernapok3_2017.jpg', '/images/bollernapok3_2017.jpg', '149');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('8', '../static/images/bollernapok4_2017.jpg', '/images/bollernapok4_2017.jpg', '149');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('9', '../static/images/bollernapok5_2017.jpg', '/images/bollernapok5_2017.jpg', '149');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('10', '../static/images/osztondij_2017.jpg', '/images/osztondij_2017.jpg', '148');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('11', '../static/images/osztondij1_2017.jpg', '/images/osztondij1_2017.jpg', '148');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('12', '../static/images/osztondij2_2017.jpg', '/images/osztondij2_2017.jpg', '148');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('13', '../static/images/osztondij3_2017.jpg', '/images/osztondij3_2017.jpg', '148');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('14', '../static/images/aracs1_2017.jpg', '/images/aracs1_2017.jpg', '146');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('15', '../static/images/aracs2_2017.jpg', '/images/aracs2_2017.jpg', '146');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('16', '../static/images/pacser_falunap1_2017.jpg', '/images/pacser_falunap1_2017.jpg', '144');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('17', '../static/images/pacser_falunap2_2017.jpg', '/images/pacser_falunap2_2017.jpg', '144');

INSERT INTO picture (pictureId, pictureStaticPath, picturePath, articleId)
VALUES
('18', '../static/images/pacser_falunap3_2017.jpg', '/images/pacser_falunap3_2017.jpg', '144');

INSERT INTO texts (textsId, texts, articleId)
VALUES
('1', 'Megkezdték az utcai betonutak javítását.', 150);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('2', 'A tordai csapat ezúttal is sikeresen szerepelt  Balástyán a böllérfesztiválon. A műsorban felléptek a Petőfi Sándor ME táncosai, énekesei valamint az Orgonavirág asszony kórus is.', 149);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('3', 'A budapesti Rákóczi Szövetség ünnepélyes keretek közt osztotta ki november 6-án az első osztályos tanulóknak az ösztöndíjat.', 148);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('4', '09:00 A Fesztivál ünnepélyes megnyitója', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('5', '09:30 Szentmise', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('6', '10:00 Lovaskocsis felvonulás a parcellához', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('7', '10:30 Családi kézműves program  a gyerekeknek ( gyöngyfűzés, kukoricából  és zöldségfélékből készítendő játékok, őszi termékek festése stb. )', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('8', '10:30 Kispályás labdarúgó torna', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('9', '12:00 A Prosperitate Alapítvány által támogatott mezőgépek bemutatója', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('10', '12:00 A hagyományos ízek főzőversenyének és a pálinka versenynek a  megkezdése', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('11', '12:00 Dr. Berényi János emléktáblájának megkoszorúzása', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('12', '13:00 Művelődési műsor', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('13', '16:00 Eredményhirdetések', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('14', '20:00 Tűzijáték', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('15', '20:00 Kukoricabál az Éjjeli mulatóval  a vadászotthonban ', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('16', 'A programok helyszíne ( kivéve a kukoricatörési versenyt)  a sportközpontban lesz.', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('17', 'A látogatókat kézműves vásár is várja és egész nap látogatható lesz a dr. Berényi János múzeum.', 147);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('18', 'A szervezők (Orgonavirág Hagyományápoló Egyesület és a Helyi közösség) a hagyományos Kukoricafesztivált az idén szeptember 16-án tartják meg. A szervezők várják a csapatok bejelentkezését a kukoricatörő valamint a hagyományos ízek főzőversenyére a 0606780743-as telefonra.', 145);

INSERT INTO texts (textsId, texts, articleId)
VALUES
('19', 'A helyi közösség küldöttsége az idén is részt vett a pacséri falunapi ünnepségen,a művelődési műsorban meg fellépett a Petőfi Sándor ME középső tánccsoportja valamint az Orgonavirág asszonykórus. A horgászversenyen és a főzőversenyen is részt vettek a tordaiak.', 144);