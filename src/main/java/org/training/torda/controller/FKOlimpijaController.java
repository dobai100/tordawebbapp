package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FKOlimpijaController {

	@RequestMapping(value = "/fk_olimpija.html")
	public String showFKOlimpija(Model model) {
		return "fk_olimpija";
	}
}
