package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class KontaktController {
	
	@RequestMapping(value = "/kontakt.html")
	public String showKontakt(Model model) {
		return "kontakt";
	}
}
