package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FotogalerijaController {
	
	@RequestMapping(value = "/fotogalerija.html")
	public String showFotogalerija(Model model) {
		return "fotogalerija";
	}
}
