package org.training.torda.service;

import java.util.List;

import org.training.torda.model.User;

public interface UserService {

	List<User> searchByLoginName(String searchTerm);
}
