package org.training.torda.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class User {
	
	@Size(max = 36)
	private String id;
	
	@NotEmpty
    @Size(max = 64)
	private String firstName;
	
	@NotEmpty
    @Size(max = 64)
	private String lastName;
	
	@NotEmpty
    @Size(max = 64)
	private String loginName;
	
	@NotEmpty
    @Size(max = 64)
	private String password;
	
	@NotEmpty
	@Email
    @Size(max = 64)
	private String email;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
	
	@NotNull
    private Action action;

    public static enum Action {
        SAVE, BACK, DELETE
    }
    
    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
	
	public User() {
		
	}
	
	public User(String id, String firstName, String lastName, String loginName, String password, String email, Date birthday) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.loginName = loginName;
		this.password = password;
		this.email = email;
		this.birthday = birthday;		
	}
	
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getFirstName() {
    	return firstName;
    }
    
    public void setFirstName(String firstName) {
    	this.firstName = firstName;
    }
    
    public String getLastName() {
    	return lastName;
    }
    
    public void setLastName(String lastName) {
    	this.lastName = lastName;
    }
    
    public String getLoginName() {
    	return loginName;
    }
    
    public void setLoginName(String loginName) {
    	this.loginName = loginName;
    }
    
    public String getPassword() {
    	return password;
    }
    
    public void setPassword(String password) {
    	this.password = password;
    }
    
    public String getEmail() {
    	return email;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }
    
    public Date getBirthday() {
    	return birthday;
    }
    
    public void setBirthday(Date birthday) {
    	this.birthday = birthday;
    }
    
    @Override
    public String toString() {
        return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", loginName=" + loginName 
        		+ ", + password=" + password + ", + email=" + email + ", birthDate=" + birthday + ", action=" + action + "]";
    }
   
}
