package org.training.torda.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {

	@Size(max = 36)
	private String id;

	@NotNull
	private String articleName;
	
	public Article() {
		
	}
	
	public Article(String id, String articleName) {
		this.id= id;
		this.articleName = articleName;
	}
	
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getArticleName() {
    	return articleName;
    }
    
    public void setArticleName(String articleName) {
    	this.articleName = articleName;
    }
	
}
