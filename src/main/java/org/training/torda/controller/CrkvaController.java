package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CrkvaController {

	@RequestMapping(value = "/crkva.html")
	public String showCrkva(Model model) {
		return "crkva";
	}
}
