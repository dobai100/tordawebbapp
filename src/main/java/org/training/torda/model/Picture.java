package org.training.torda.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Picture {
	
	@Size(max = 36)
	private String pictureId;

	@NotNull
	private String picturePath;
	
	@NotNull
	private String pictureStaticPath;
	
	@NotNull
	private String articleId;
	
	public Picture() {
		
	}
	
	public Picture(String pictureId, String picturePath, String pictureStaticPath, String articleId) {
		this.pictureId= pictureId;
		this.picturePath = picturePath;
		this.pictureStaticPath = pictureStaticPath;
		this.articleId = articleId;
	}
	
	public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }
    
    public String getPicturePath() {
    	return picturePath;
    }
    
    public void setPicturePath(String picturePath) {
    	this.picturePath = picturePath;
    }
    
    public String getPictureStaticPath() {
    	return picturePath;
    }
    
    public void setPictureStaticPath(String pictureStaticPath) {
    	this.pictureStaticPath = pictureStaticPath;
    }
    
    public String getArticleId() {
    	return articleId;
    }
    
    public void setArticleId(String articleId) {
    	this.articleId = articleId;
    }
}
