package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LekovitaITermalnaVodaController {

	@RequestMapping(value = "/lekovita_i_termalna_voda.html")
	public String showLekovitaITermalnaVoda(Model model) {
		return "lekovita_i_termalna_voda";
	}
}
