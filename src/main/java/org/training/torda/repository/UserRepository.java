package org.training.torda.repository;

import static org.springframework.util.Assert.notNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.training.torda.model.User;

@Repository
public class UserRepository implements CRUDRepository<User, String>{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	private static final String userList  = "SELECT id, firstName, lastName, loginName, email, birthday FROM User ORDER BY id";

    private RowMapper<User> rowMapper = new RowMapper<User>() {
        @Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User e = new User();
            e.setId(rs.getString("id"));
            e.setFirstName(rs.getString("firstName"));
            e.setLastName(rs.getString("lastName"));
            e.setLoginName(rs.getString("loginName"));
            e.setPassword(rs.getString("password"));
            e.setEmail(rs.getString("email"));
            e.setBirthday(rs.getDate("birthday"));
            return e;
        }
    };
    
    @Override
	public List<User> findAll() {
        return jdbcTemplate.query("SELECT id, firstName, lastName, loginName, password, email, birthday FROM User ORDER BY id",
                new Object[] {}, rowMapper);
    }
    
    /*@Override
    public List<User> findAll(){
        return jdbcTemplate.queryForList("SELECT id, firstName, lastName, loginName, email, birthday FROM User ORDER BY id", User.class);
    }*/

    @Override
    public void create(User e) {

        notNull(e, "The entity can not be null");

        if (e.getId() == null) {
            e.setId(UUID.randomUUID().toString());
        }
        jdbcTemplate.update("INSERT INTO employee (id, firstName, lastName, loginName, password, email, birthdate) VALUES (?, ?, ?, ?, ?, ?, ?)", 
        		e.getId(), e.getFirstName(), e.getLastName(), e.getLoginName(), e.getPassword(), e.getEmail(), e.getBirthday());
    }


	@Override
	public User getById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public User getByLoginName(String loginName) {
			
		return null;
	}


	@Override
	public void update(User e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		
	}
    
}
