package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TorontaltordaFondacijaZaRazvojSelaController {	

	@RequestMapping(value = "/torontaltorda_fondacija_za_razvoj_sela.html")
	public String showTorontaltordaOrganizacijaZaRazvojSela(Model model) {
		return "torontaltorda_fondacija_za_razvoj_sela";
	}
}
