package org.training.torda.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.training.torda.repository.UserRepository;
import org.training.torda.model.User;

@Controller
public class LoginController {
		
	@Autowired
    private UserRepository userRepository;
		
		
	  @RequestMapping(value = "Admin/login", method = RequestMethod.GET)

	  public String showLogin(Model model, @ModelAttribute("loginform") User loginform, @RequestParam(required = false) String loginName) {

	    model.addAttribute("loginForm", loginform);

	    return "Admin/login";

	  }
	
		
	@RequestMapping(value = "Admin/login", method = RequestMethod.POST)
	public String loginForm(Model model, @Valid @ModelAttribute("loginform") User loginform, BindingResult bindingLoginResult) {
		
		if (bindingLoginResult.hasErrors()) {
			return "Admin/login";
		}

		return "redirect:/userlists.html";
	}
		
}
