package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LovackoDrustvoFazanController {

	@RequestMapping(value = "/lovacko_drustvo_fazan.html")
	public String showSLovackoDrustvoFazan(Model model) {
		return "lovacko_drustvo_fazan";
	}
}
