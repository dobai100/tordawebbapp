package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UdruzenjeZaNegovanjeTradicijeJorgovanController {

	@RequestMapping(value = "/udruzenje_za_negovanje_tradicije_jorgovan.html")
	public String showUdruzenjeZaNegovanjeTradicijeJorgovan(Model model) {
		return "udruzenje_za_negovanje_tradicije_jorgovan";
	}
}
