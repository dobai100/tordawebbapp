package org.training.torda.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.training.torda.repository.UserRepository;
import org.training.torda.model.User;

@Controller
@RequestMapping(value = "/Admin/registration")
public class RegistrationController {
	
	@Autowired
    private UserRepository userRepository;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String showRegistration(Model model, @ModelAttribute("form") User form) {
		model.addAttribute("registrationForm", new User());
		return "Admin/registration";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String registrationForm(@ModelAttribute("form") @Valid User form, BindingResult bindingResult) {
		
		switch (form.getAction()) {

        case SAVE:

            if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
                return "Admin/registration";
            }

            User user = new User();

            userRepository.create(user);
                
            break;

        case DELETE:
            userRepository.deleteById(form.getId());
            break;
        }

		return "Admin/admin";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class,  new StringTrimmerEditor(true /* empty as null */));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd/MM/yyyy"), true /*allow empty */));
	}

}
