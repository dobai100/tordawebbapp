package org.training.torda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TordaWebbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TordaWebbappApplication.class, args);
	}
}
