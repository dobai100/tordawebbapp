package org.training.torda.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.training.torda.model.Article;
import org.training.torda.model.Picture;
import org.training.torda.model.Texts;

@Controller
public class ArticleRepository {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public List<Article> findLastFive() {
        return jdbcTemplate.query("SELECT id, articleName FROM Article ORDER BY id DESC LIMIT 5",
                new Object[] {}, rowMapper);
    };
    
    public List<Article> findAll() {
        return jdbcTemplate.query("SELECT id, articleName FROM Article ORDER BY id DESC",
                new Object[] {}, rowMapper);
    };
    
    public List<Picture> findPictureForArticleId() {
    	return jdbcTemplate.query("SELECT Article.id, Article.articleName,"
    			+ " Picture.pictureId, Picture.picturePath, Picture.pictureStaticPath, Picture.articleId FROM Article "
    			+ "INNER JOIN  Picture ON Article.id=Picture.articleId ORDER BY CAST(Picture.pictureId AS INT)",
                new Object[] {}, rowMapper1);
    }
    
    public List<Texts> findTextsForArticleId() {
    	return jdbcTemplate.query("SELECT Article.id, Article.articleName,"
    			+ " Texts.textsId, Texts.texts, Texts.articleId FROM Article "
    			+ "INNER JOIN  Texts ON Article.id=Texts.articleId ORDER BY CAST(Texts.textsId AS INT)",
                new Object[] {}, rowMapper2);
    }
	
	private RowMapper<Article> rowMapper = new RowMapper<Article>() {
        @Override
		public Article mapRow(ResultSet rs, int rowNum) throws SQLException {
            Article e = new Article();
            e.setId(rs.getString("id"));
            e.setArticleName(rs.getString("articleName"));
            return e;
        }
    };
    
    private RowMapper<Picture> rowMapper1 = new RowMapper<Picture>() {
        @Override
		public Picture mapRow(ResultSet rs, int rowNum) throws SQLException {
            Picture e = new Picture();
            e.setPictureId(rs.getString("pictureId"));
            e.setPicturePath(rs.getString("picturePath"));
            e.setPictureStaticPath(rs.getString("pictureStaticPath"));
            e.setArticleId(rs.getString("articleId"));
            return e;
        }
    };
    
    private RowMapper<Texts> rowMapper2 = new RowMapper<Texts>() {
        @Override
		public Texts mapRow(ResultSet rs, int rowNum) throws SQLException {
            Texts e = new Texts();
            e.setTextsId(rs.getString("textsId"));
            e.setTexts(rs.getString("texts"));
            e.setArticleId(rs.getString("articleId"));
            return e;
        }
    };

}
