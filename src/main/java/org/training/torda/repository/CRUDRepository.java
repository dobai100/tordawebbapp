package org.training.torda.repository;

import java.util.List;

public interface CRUDRepository<E, K> {

	public void create(E e);

    public E getById(K id);
    
    public E getByLoginName(K loginName);
    
    public List<E> findAll();

    public void update(E e);

    public void deleteById(K id);

}
