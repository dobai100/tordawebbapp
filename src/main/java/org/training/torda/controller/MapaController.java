package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MapaController {

	@RequestMapping(value = "/mapa.html")
	public String showMapa(Model model) {
		return "mapa";
	}
}
