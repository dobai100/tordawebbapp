package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/Admin")
public class AdminController {
	
	@RequestMapping
    public String showAdmin() {
        return "Admin/admin";
    }

}
