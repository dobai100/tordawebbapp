package org.training.torda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.training.torda.repository.ArticleRepository;

@Controller
public class VestiController {

	@Autowired
    private ArticleRepository articleRepository;
	
	@RequestMapping(value = "/vesti.html")
	public String showVesti(Model model) {
		
		model.addAttribute("articles", articleRepository.findAll());
		model.addAttribute("texts", articleRepository.findTextsForArticleId());
		model.addAttribute("pictures", articleRepository.findPictureForArticleId());
		
		return "vesti";
	}
}
