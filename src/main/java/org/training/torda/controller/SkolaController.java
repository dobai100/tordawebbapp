package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SkolaController {

	@RequestMapping(value = "/skola.html")
	public String showSkola(Model model) {
		return "skola";
	}
}
