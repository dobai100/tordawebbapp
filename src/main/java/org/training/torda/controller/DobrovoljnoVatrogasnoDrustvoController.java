package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DobrovoljnoVatrogasnoDrustvoController {

	@RequestMapping(value = "/dobrovoljno_vatrogasno_drustvo.html")
	public String showDobrovoljnoVatrogasnoDrustvo(Model model) {
		return "dobrovoljno_vatrogasno_drustvo";
	}
}
