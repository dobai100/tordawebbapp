package org.training.torda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AracDustvoZaNegovanjeTradicijeController {
	
	@RequestMapping(value = "/arac_dustvo_za_negovanje_tradicije.html")
	public String showAracDustvoZaNegovanjeTradicije(Model model) {
		return "arac_dustvo_za_negovanje_tradicije";
	}
}
