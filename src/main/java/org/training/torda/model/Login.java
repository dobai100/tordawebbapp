package org.training.torda.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class Login {

	@NotEmpty
    @Size(max = 64)  	
	private String loginName;

	@NotEmpty
    @Size(max = 64)
	private String password;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
    public String toString() {
        return "User [loginName=" + loginName + ", + password=" + password + "]";
    }
}
